# Document Signer

`Please check dedocco_assignment.mov video for easy understanding`

## Document Signer

This project covers following scenarios:-

1. User login and authentication and (authorization upto certain extents) using JWT
2. Showing Pending and submitted documents on dashboard
3. Create document project flow

- e.g.
  Step 1. User_A will upload document(e.g. DocumentX) in project
  Step 2. User_A adds recepient(e.g. User_b) for each document (name,email)
  Step 3. User_A adds Message to explain the reason for sending documents & what action recepients needs to take

4. Opening documents sent by others using link or API provided in Email

- User_B upon receiving document in email can open it using the link and mark it signed and submit
- If User_B is Registered under system then he can see the pending documents on dashboard page

## Architecture:-

MERN stack

## How to build to test in local

- Install Mongo DB & run mongo db
- Download GIT project
- cd docu-signer-backend and run 'npm install' & then 'npm start'
- cd docu-signer-frontend and run 'npm install' & then 'npm start'
- Open http://localhost:3000/

## Video

PFA video for understanding the usage.
`Please check dedocco_assignment.mov video for easy understanding`
