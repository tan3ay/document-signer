import * as React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../state/store";
import { Document } from "../../model/document";
import { Email } from "../../state/emailReducer";
import { Dialog, DialogContent } from "@mui/material";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router";

import "./index.scss";

interface EmailViewProps {}
const EmailView: React.FC<EmailViewProps> = () => {
  const documents: Document[] = useSelector(
    (state: RootState) => state.newPrpject.project.documents
  );
  const email: Email = useSelector((state: RootState) => state.email);
  const [open, setOpen] = React.useState(false);
  const [document, setDocument] = React.useState<Document | null>(null);
  const history = useHistory();
  const handleClickOpen = React.useCallback(() => {
    setOpen(true);
  }, []);
  const handleClose = React.useCallback(() => {
    setOpen(false);
  }, []);
  const handleClickView = React.useCallback(
    (docId) => {
      const target = documents.find(({ id }) => id === docId);
      setDocument(target!);
      handleClickOpen();
    },
    [documents, handleClickOpen]
  );
  const mailList = documents.map((d) => <div key={d.id}>{d.signerEmail}</div>);
  const mailInfo = documents.map((d) => (
    <div key={d.id} className="list-item-container">
      <div className="text">
        Email sent to "{d.signerName}" for file "{d.name}"
      </div>
      <button className="btn primary" onClick={() => handleClickView(d.id)}>
        View
      </button>
    </div>
  ));
  return (
    <div className="emailView">
      <div
        className="link"
        onClick={(e) => {
          history.replace("/dashboard");
        }}
      >
        <FontAwesomeIcon icon={faAngleLeft} /> Return to dashboard
      </div>
      <div className="title">E-Mail sent successfully to all recepients.</div>
      <div className="content">
        {/* <div className="subtitle"> Mail List </div>
        <div className="list">{mailList}</div> */}
        <div className="subtitle">Detail: </div>
        <div className="list">{mailInfo}</div>
      </div>

      <Dialog open={open && !!document} onClose={handleClose}>
        <DialogContent>
          <div className="dialog">
            <div className="header">
              <span>To:</span> {document?.signerName || ""}
            </div>
            <div className="header">
              <span>Subject:</span>
              {email.subject}
            </div>

            <div className="header">Message:</div>
            <div className="detail">{email.message}</div>
            <div className="header">
              <span className="field">Attachment:</span>
              <a
                href={`http://localhost:3000/docView/${document?.id}/true`}
                target="_blank"
                rel="noreferrer"
              >
                {document?.name || "some file"}
              </a>
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
};
export default React.memo(EmailView);
