import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from "@mui/material";
import { FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Document } from "../../../model/document";

import { documentRemoved, onProcessAdded } from "../../../state/projectReducer";
import { RootState } from "../../../state/store";
import { AddProcessDialog } from "../AddProcess";
import {
  DocumentListItem,
  DOCUMENT_LIST_ITEM_TYPE,
} from "../components/DocumentListItem";

export const DefineProcess: FC<{
  onBack: () => void;
  onSubmit: () => void;
}> = ({ onBack, onSubmit }) => {
  const documents: Document[] = useSelector(
    (state: RootState) => state.newPrpject.project.documents
  );
  if (documents.length === 0) {
    onBack();
  }
  const [openAddProcess, setOpenAddProcess] = useState(false);

  const [selectedDocument, setSelectedDocument] = useState<Document>();
  const [email, setSignerEmail] = useState("");
  const [name, setSignerName] = useState("");
  const [invalidEmailErr, setInvalidEmailErr] = useState(false);
  const [invalidNameErr, setInvalidNameErr] = useState(false);

  const dispatch = useDispatch();
  const history = useHistory();

  const handleListItemAddProcess = (id: string) => {
    const target = documents.find((item) => item.id === id);
    setSelectedDocument(target);
    setSignerName(target?.signerName || "");
    setSignerEmail(target?.signerEmail || "");
    setOpenAddProcess(true);
  };

  const handleListItemDelete = (id: string) => {
    const removedDocument = documents.find((item) => item.id === id);
    if (removedDocument) dispatch(documentRemoved(removedDocument));
  };

  const handleClose = () => {
    setOpenAddProcess(false);
  };

  const handleProcessSave = () => {
    if (isDialogInputValid()) {
      if (selectedDocument) {
        const currentDocument = { ...selectedDocument };
        currentDocument.signerEmail = email;
        currentDocument.signerName = name;
        setSignerEmail("");
        setSignerName("");
        dispatch(onProcessAdded(currentDocument));
      }

      setOpenAddProcess(false);
    }
  };

  const documentList = documents.map((document) => {
    return (
      <DocumentListItem
        id={document.id}
        key={document.id}
        document={document}
        type={DOCUMENT_LIST_ITEM_TYPE.COMPLEX}
        onButtonClick={handleListItemAddProcess}
        onIconClick={handleListItemDelete}
      />
    );
  });

  const isDialogInputValid = (): boolean => {
    let isValid = true;
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email === "" || !email.match(mailformat)) {
      setInvalidEmailErr(true);
      isValid = false;
    } else {
      setInvalidEmailErr(false);
    }

    if (name === "") {
      setInvalidNameErr(true);
      isValid = false;
    } else {
      setInvalidNameErr(false);
    }
    return isValid;
  };

  const documentProcessValid = (): boolean => {
    let isValid = true;
    documents.forEach((document) => {
      if (document.signerEmail === "") isValid = false;
    });
    return isValid;
  };

  return (
    <div className="DefineProcess">
      <div className="header">Defined document process</div>
      <div className="details">
        Configure the signature placement(s) and sequence of document(s) invoved
        int this project
      </div>
      {!documentProcessValid() && (
        <p className="error">
          {" "}
          {"One or more document dont have recepient added to it"}{" "}
        </p>
      )}
      <div className="document-list">{documentList}</div>
      <div className="button-container">
        <button className="btn" onClick={(e) => onBack()}>
          BACK
        </button>
        <button
          className="btn"
          onClick={(e) => {
            if (documentProcessValid()) {
              onSubmit();
            }
          }}
        >
          CONTINUE
        </button>
      </div>

      <Dialog open={openAddProcess} onClose={handleClose}>
        <DialogTitle>Add Process</DialogTitle>
        <DialogContent>
          <DialogContentText>{selectedDocument?.name}</DialogContentText>
          <DialogContentText>Enter recepient details:</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            type="name"
            fullWidth
            variant="standard"
            value={name}
            onChange={(e) => setSignerName(e.target.value)}
          />
          {invalidNameErr && (
            <p className="error"> {"Please enter valid name"} </p>
          )}
          <TextField
            margin="dense"
            id="name"
            label="Email Address"
            type="email"
            fullWidth
            variant="standard"
            value={email}
            onChange={(e) => setSignerEmail(e.target.value)}
          />
          {invalidEmailErr && (
            <p className="error"> {"Please enter valid email address"} </p>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleProcessSave}>Save</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
