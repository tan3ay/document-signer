import { FC, useState } from "react";
import { NavBar } from "../../components/NavBar";
import { CreateProject } from "./CreateProject";
import { DefineProcess } from "./DefineProcess";
import { SubmitProject } from "./SubmitProject";
import "./index.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router";
import { Document } from "../../model/document";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../state/store";
import { DocumentAPI } from "../../api/document-api";
import { Project } from "../../model/project";

export const DocumentApplication: FC = () => {
  const [step, setStep] = useState(1);

  const history = useHistory();
  const documents: Document[] = useSelector(
    (state: RootState) => state.newPrpject.project.documents
  );
  const dispatch = useDispatch();
  const handleBackButtonClick = function () {
    setStep(step - 1);
  };
  const handleContinueButtonClick = function () {
    setStep(step + 1);
  };
  const handleSubmitButtonClick = function () {
    DocumentAPI.postDocuments(documents);
  };

  const getButtonClass = (current: number, target: number) => {
    if (target === current) {
      return "primary";
    }
    if (current > target) {
      return "finished";
    } else {
      return "default";
    }
  };

  return (
    <main className="DocumentApplication">
      <div>
        <NavBar />
      </div>
      <div className="content">
        <div className="left-panel">
          <div
            className="link"
            onClick={(e) => {
              history.replace("/dashboard");
            }}
          >
            <FontAwesomeIcon icon={faAngleLeft} /> Return to dashboard
          </div>
          <div className="header">New Project</div>
          <button className={`btn ${getButtonClass(step, 1)}`}>
            Add Document(s)
          </button>
          <button className={`btn ${getButtonClass(step, 2)}`}>
            Add Process
          </button>
          <button className={`btn ${getButtonClass(step, 3)}`}>
            Confirmation
          </button>
        </div>
        <div className="right-panel">
          {step == 1 && (
            <CreateProject
              onBack={handleBackButtonClick}
              onSubmit={handleContinueButtonClick}
            />
          )}
          {step == 2 && (
            <DefineProcess
              onBack={handleBackButtonClick}
              onSubmit={handleContinueButtonClick}
            />
          )}
          {step == 3 && (
            <SubmitProject
              onBack={handleBackButtonClick}
              onSubmit={handleSubmitButtonClick}
            />
          )}
          {/* <div className="button-container">
                        {(step==2||step==3) && <button className="btn" onClick={(e)=>handleBackButtonClick(step)}>BACK</button>}
                        {(step==1||step==2) && <button className="btn" onClick={(e)=>handleContinueButtonClick(step)}>CONTINUE</button>}
                        {(step==3) && <button className="btn" onClick={(e)=>saveDocuments()}>REGISTER TO LEDGER</button>}
                    </div> */}
        </div>
      </div>
    </main>
  );
};
