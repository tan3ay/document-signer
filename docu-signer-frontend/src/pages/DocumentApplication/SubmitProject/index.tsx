import { TextField } from "@mui/material";
import { FC, useCallback, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { DocumentAPI } from "../../../api/document-api";
import { Document } from "../../../model/document";
import { RootState } from "../../../state/store";
import { DocumentListItem } from "../components/DocumentListItem";
import { useHistory } from "react-router";
import { updateEmail } from "../../../state/emailReducer";
import { submitProject } from "../../../state/projectReducer";
import { Project } from "../../../model/project";

export const SubmitProject: FC<{
  onBack: () => void;
  onSubmit: () => void;
}> = ({ onBack, onSubmit }) => {
  const documents: Document[] = useSelector(
    (state: RootState) => state.newPrpject.project.documents
  );
  const project: Project = useSelector(
    (state: RootState) => state.newPrpject.project
  );
  const history = useHistory();
  const dispatch = useDispatch();
  const [subject, setSubject] = useState("");
  const [message, setMessage] = useState("");
  const [invalidEmailErr, setInvalidEmailErr] = useState(false);
  const documentList = documents.map((document) => {
    return (
      <DocumentListItem
        id={document.id}
        key={document.id}
        document={document}
      />
    );
  });

  const isInputValid = () => {
    if (subject.trim() === "" || message.trim() === "") {
      return false;
    }
    return true;
  };
  const handleSubmit = useCallback(async () => {
    if (isInputValid()) {
      setInvalidEmailErr(false);
      dispatch(updateEmail({ subject, message }));
      dispatch(submitProject(project));
      history.push("/emailView");
    } else {
      setInvalidEmailErr(true);
    }
  }, [dispatch, history, message, subject]);

  return (
    <div className="SubmitProject">
      <div className="content">
        <div className="left">
          <div className="header">Confirmation</div>
          <div className="details">
            Please ensure that the document process and receipient(s) are added
            correctly before you click register to proceed.
          </div>
          {invalidEmailErr && (
            <p className="error">
              {"Please enter subject and message for recepients"}
            </p>
          )}
          <div className="message-container">
            <div className="subject">
              <TextField
                fullWidth
                id="subject"
                label="Subject"
                onChange={(e) => setSubject(e.target.value)}
              />
            </div>
            <div className="message">
              <TextField
                fullWidth
                id="message"
                label="Message"
                multiline
                rows={4}
                onChange={(e) => setMessage(e.target.value)}
              />
            </div>
          </div>
        </div>
        <div className="right">
          <div className="header">Projeect Summery</div>
          <div className="document-list">{documentList}</div>
        </div>
      </div>
      <div className="button-container">
        <button className="btn" onClick={(e) => onBack()}>
          BACK
        </button>
        <button className="btn" onClick={handleSubmit}>
          REGISTER TO LEDGER
        </button>
      </div>
    </div>
  );
};
