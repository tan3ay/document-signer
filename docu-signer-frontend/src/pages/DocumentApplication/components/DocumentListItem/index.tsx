import { faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Switch } from "@mui/material";
import { FC } from "react";
import { useDispatch } from "react-redux";
import { Document } from "../../../../model/document";
import { documentUpdated } from "../../../../state/projectReducer";

import "./index.scss";

export enum DOCUMENT_LIST_ITEM_TYPE {
  SIMPLE = "SIMPLE",
  COMPLEX = "COMPLEX",
}

interface IDocumentListItem {
  id: string;
  document: Document;
  type?: DOCUMENT_LIST_ITEM_TYPE;
  onButtonClick?: (id: string) => void;
  onIconClick?: (id: string) => void;
}
//TODO refactor
export const DocumentListItem: FC<IDocumentListItem> = ({
  id,
  document,
  type,
  onButtonClick,
  onIconClick,
}) => {
  const dispatch = useDispatch();

  if (type === undefined) {
    type = DOCUMENT_LIST_ITEM_TYPE.SIMPLE;
  }

  const handleButtonClick = (id: string) => {
    if (onButtonClick) {
      onButtonClick(id);
    }
  };

  const handleIconClick = (id: string) => {
    if (onIconClick) {
      onIconClick(id);
    }
  };

  return (
    <div className="DocumentListItem">
      {type === DOCUMENT_LIST_ITEM_TYPE.SIMPLE && (
        <div className="item-container">
          <div className="left">
            <div className="file-name">{document.name}</div>
          </div>
          <div className="right">
            {onIconClick && (
              <div
                className="icon"
                onClick={(e) => {
                  handleIconClick(id);
                }}
              >
                <FontAwesomeIcon icon={faTrashAlt} />
              </div>
            )}
          </div>
        </div>
      )}
      {type === DOCUMENT_LIST_ITEM_TYPE.COMPLEX && (
        <div className="item-container">
          <div className="left">
            <div className="file-name">{document.name}</div>
            <div className="detail">Required Signature</div>
            <div className="switch">
              <Switch
                checked={document.isSignatureRequired}
                name="gilad"
                onClick={() => {
                  document.isSignatureRequired = !document.isSignatureRequired;
                  document.signerEmail = "";
                  document.signerName = "";
                  dispatch(documentUpdated(document));
                }}
              />
            </div>
          </div>
          <div className="right">
            {document.signerEmail === "" && (
              <button
                className="btn primary"
                onClick={(e) => {
                  handleButtonClick(id);
                }}
              >
                Process
              </button>
            )}
            {document.signerEmail !== "" && (
              <button
                className="btn primary"
                onClick={(e) => {
                  handleButtonClick(id);
                }}
              >
                Update
              </button>
            )}
            <div
              className="icon"
              onClick={(e) => {
                handleIconClick(id);
              }}
            >
              <FontAwesomeIcon icon={faTrashAlt} />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
