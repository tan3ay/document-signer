import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { TextField } from "@mui/material";
import { ChangeEvent, ChangeEventHandler, FC, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Document } from "../../../model/document";
import { Project } from "../../../model/project";
import {
  documentAdded,
  documentRemoved,
  projectUpdated,
} from "../../../state/projectReducer";
import { RootState } from "../../../state/store";
import { DocumentListItem } from "../components/DocumentListItem";

export const CreateProject: FC<{
  onBack: () => void;
  onSubmit: () => void;
}> = ({ onBack, onSubmit }) => {
  const dispatch = useDispatch();
  const documents: Document[] = useSelector(
    (state: RootState) => state.newPrpject.project.documents
  );
  const userEmail: string = useSelector((state: RootState) => state.user.email);
  const currentProject: Project = useSelector(
    (state: RootState) => state.newPrpject.project
  );

  const hiddenFileInput: React.LegacyRef<HTMLInputElement> = useRef(null);
  const handleUploadClick = () => {
    if (hiddenFileInput.current) hiddenFileInput.current.click();
  };
  const handleFileUpload = (event: ChangeEvent<HTMLInputElement>) => {
    if (event != null && event.target != null && event.target.files != null) {
      const fileUploaded = event.target.files[0];
      // TODO: Process upload file action
      // For simplicity I would be only saving metadata about file in DB
      if (!fileUploaded.name.endsWith(".pdf")) {
        setFileUploadErr(true);
      } else {
        setFileUploadErr(false);
        dispatch(documentAdded(userEmail, fileUploaded.name));
      }
    }
  };

  const handleListItemDelete = (id: string) => {
    const removedDocument = documents.find((item) => item.id === id);
    if (removedDocument) dispatch(documentRemoved(removedDocument));
  };

  const documentList = documents.map((document) => {
    return (
      <DocumentListItem
        id={document.id}
        key={document.id}
        document={document}
        onIconClick={handleListItemDelete}
      />
    );
  });

  const [projectName, setProjectName] = useState(currentProject.name);

  const [projectNameErr, setProjectNameErr] = useState(false);
  const [zeroDocumentErr, setZeroDocumentErr] = useState(false);
  const [fileUploadErr, setFileUploadErr] = useState(false);
  const isInputValid = () => {
    let isValid = true;
    if (projectName == "") {
      setProjectNameErr(true);
      isValid = false;
    }
    if (currentProject.documents.length === 0) {
      setZeroDocumentErr(true);
      isValid = false;
    }
    return isValid;
  };

  return (
    <div className="CreateProject">
      <div className="header">Create New Project</div>
      <div className="details">
        Set up name and document(s) needed for this project
      </div>
      <TextField
        value={projectName}
        variant="outlined"
        onChange={(e) => {
          // currentProject.name = e.target.value;
          // dispatch(projectUpdated(currentProject));
          setProjectName(e.target.value);
        }}
      />
      {projectNameErr && <p className="error"> {"Invalid project name"} </p>}

      <div className="sub-header">DOCUMENT</div>
      <div>Choose the document(s) to start the project with</div>
      <div className="upload-document" onClick={() => handleUploadClick()}>
        <FontAwesomeIcon icon={faPlus} />
      </div>
      {fileUploadErr && (
        <p className="error"> {"Only pdf files are supported"} </p>
      )}
      <input
        type="file"
        ref={hiddenFileInput}
        onChange={handleFileUpload}
        style={{ display: "none" }}
      ></input>
      <div className="sub-header">Uploaded Documents</div>
      {zeroDocumentErr && (
        <p className="error"> {"Project must contain atleast one document"} </p>
      )}
      <div className="document-list">{documentList}</div>
      <div className="button-container">
        <button
          className="btn"
          onClick={(e) => {
            if (isInputValid()) {
              currentProject.name = projectName;
              currentProject.documents.forEach(
                (document) => (document.projectName = projectName)
              );
              dispatch(projectUpdated(currentProject));
              onSubmit();
            }
          }}
        >
          CONTINUE
        </button>
      </div>
    </div>
  );
};
