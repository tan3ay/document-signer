import { TextField } from "@mui/material";
import React, { FC, useContext, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { UserApi } from "../../api/user-api";
import { onUserLoggedIn } from "../../state/userReducer";

import "./index.scss";

export const LoginPage: FC<{ onLoginSuccess?: () => void }> = ({
  onLoginSuccess,
}) => {
  const history = useHistory();
  const [loginFailed, setLoginFailed] = useState<boolean>(false);
  const dispatch = useDispatch();
  const [loginError, setLoginError] = useState("");
  const [isError, setError] = useState();
  const handleSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    const target = e.target as typeof e.target & {
      email: { value: string };
      password: { value: string };
    };

    const email = target.email.value; // typechecks!
    const password = target.password.value; // typechecks!

    if (email === "" || password === "") {
      setLoginFailed(true);
      setLoginError("Please enter email & password");
      return;
    } else {
      setLoginFailed(false);
    }

    // validate email & password
    try {
      const res = await UserApi.login(email, password);
      if (res.data.token) {
        localStorage.setItem(
          "login",
          JSON.stringify({
            login: true,
            token: res.data.token,
          })
        );

        const userData = JSON.parse(atob(res.data.token.split(".")[1]));
        if (onLoginSuccess) {
          onLoginSuccess();
        }
        dispatch(
          onUserLoggedIn({
            email: userData.email,
            name: userData.name,
            loginSuccess: true,
          })
        );
        setLoginFailed(false);
        history.push("/dashboard");
      } else {
        setLoginFailed(true);
      }
    } catch (error) {
      setLoginFailed(true);
    }
  };

  return (
    <main className="LoginPage">
      <div className="login-panel">
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="input-form">
            <div className="text-field">
              <TextField
                type="text"
                placeholder="Email"
                name="email"
                fullWidth
              />
            </div>
            <div className="text-field">
              <TextField
                type="password"
                placeholder="PassWord"
                name="password"
                fullWidth
              />
            </div>
          </div>
          {loginFailed && (
            <p className="error"> {"Invalid email or password."} </p>
          )}
          <div className="button-container">
            <button className="btn primary fullwidth" type="submit">
              Login
            </button>
          </div>
        </form>
      </div>
    </main>
  );
};
