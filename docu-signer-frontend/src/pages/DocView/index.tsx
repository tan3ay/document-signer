import * as React from "react";
import { useSelector } from "react-redux";
import { Document } from "../../model/document";
import { Checkbox, Button } from "@mui/material";
import { RouteComponentProps } from "react-router";
import "./index.scss";
import { DocumentAPI } from "../../api/document-api";

interface MatchParams {
  documentId: string;
  isSinger: string;
}

interface DocViewProps extends RouteComponentProps<MatchParams> {}
const DocView: React.FC<DocViewProps> = ({
  match,
}: RouteComponentProps<{ documentId?: string; isSinger?: string }>) => {
  const [document, setDocument] = React.useState<Document | null>(null);
  const [checked, setChecked] = React.useState(false);
  const [submitted, setSubmitted] = React.useState(false);
  const [signed, setSigned] = React.useState(false);
  const isSinger = match.params?.isSinger === "true";

  const handleSubmit = React.useCallback(async () => {
    const documentId = match.params?.documentId!;
    if (documentId) {
      try {
        const res = await DocumentAPI.patchDocument(documentId, {
          status: "signed",
        });
        setSubmitted(true);
      } catch {
        setSubmitted(false);
      }
      //TODO consider promise then: when submit api successfully, update this state; otherwise error msg and maybe retry
    }
  }, []);

  React.useEffect(() => {
    const documentId = match.params?.documentId!;
    if (documentId) {
      //TODO replace this by calling API to fetch document data
      // if document already sined, setSubmitted(true)
      const loadDocument = async () => {
        const document: Document = (
          await DocumentAPI.getDocumentById(documentId)
        ).data;
        setDocument(document);
        if (document.status == "signed") {
          setSigned(true);
        }
      };
      loadDocument();
    }
  }, [match]);

  return (
    <div className="DocView">
      <div className="container">
        {submitted ? (
          <div className="header">Submitted singed documents Successfully!</div>
        ) : (
          <>
            <div className="header">File Name: {document?.name}</div>
            <div className="header">
              {isSinger
                ? `Creator: ${document?.ownerEmail}`
                : `Receiver: ${document?.signerName}`}
            </div>

            {isSinger ? (
              signed ? (
                <div className="header">
                  You have already signed the document. Thank you.
                </div>
              ) : (
                <div className="header">
                  <div>
                    <Checkbox
                      checked={checked}
                      onChange={() => setChecked(!checked)}
                    />
                    <span>Check to sign</span>
                  </div>
                  <Button
                    className="btn "
                    variant="contained"
                    disabled={!checked}
                    onClick={handleSubmit}
                  >
                    Submit
                  </Button>
                </div>
              )
            ) : null}
          </>
        )}
      </div>
    </div>
  );
};
export default React.memo(DocView);
