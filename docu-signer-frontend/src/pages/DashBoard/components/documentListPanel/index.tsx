import React, { FC, useCallback } from "react";
import { useHistory } from "react-router";
import { Document } from "../../../../model/document";
import { Column, DocumentDataTable } from "../documentTable";
import { Button } from "@mui/material";

import "./index.scss";
interface IDocumentListPanel {
  header: string;
  details: string;
  columnHeaders: Column[];
  documentData: Document[];
  isAssignedList?: boolean;
}

export const DocumentListPanel: FC<IDocumentListPanel> = ({
  header,
  details,
  columnHeaders,
  documentData,
  isAssignedList,
}) => {
  const history = useHistory();
  const handleBtnClick = useCallback(
    (id: string) => {
      history.push(`/docView/${id}/${isAssignedList ? true : false}`);
    },
    [history, isAssignedList]
  );
  const renderActionBtn = (id: string) => (
    <Button variant="contained" onClick={() => handleBtnClick(id)}>
      {isAssignedList ? "Sign Document" : "View Document"}
    </Button>
  );

  return (
    <div className="DocumentListPanel">
      <div className="header-container">
        <div className="header">{header}</div>
        <div className="detail">{details}</div>
      </div>
      <div className="table-container">
        <DocumentDataTable
          headers={columnHeaders}
          documentData={documentData}
          renderActionBtn={renderActionBtn}
        />
      </div>
    </div>
  );
};
