const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

const getUnit = (n: number, unit: string) => (n > 1 ? unit + "s" : unit);

export const getTimeLabel = (date: number) => {
  const timestamp = new Date(date);

  // check date validity
  if (!isNaN(timestamp.getTime())) {
    const now = new Date();
    const diffTime = Math.abs(now.valueOf() - timestamp.valueOf());
    const diffMinute = Math.floor(diffTime / (1000 * 60));
    const diffHours = Math.floor(diffTime / (1000 * 60 * 60));
    if (diffTime / (1000 * 60) < 1) {
      // just now - within 1 mins
      return "just now";
    } else if (diffTime / (1000 * 60 * 60) < 1) {
      return `${diffMinute} ${getUnit(diffMinute, "minute")} ago`;
    } else if (diffTime / (1000 * 60 * 60 * 24) < 1) {
      return `${diffHours} ${getUnit(diffMinute, "hour")} ago`;
    } else {
      // template - 22 July 2020
      return `${timestamp.getDate()} ${
        monthNames[timestamp.getMonth()]
      } ${timestamp.getFullYear()}`;
    }
  }
  return "-";
};
