import React, { FC } from "react";
import { Document, getKeyValue } from "../../../../model/document";
import { getTimeLabel } from "./utils";

import "./index.scss";

export interface Column {
  id: string;
  title: string;
  width: string;
}
export interface IDocumentListPanel {
  headers: Column[];
  documentData: Document[];
  renderActionBtn: (id: string) => React.ReactElement;
}

export const DocumentDataTable: FC<IDocumentListPanel> = ({
  headers,
  documentData,
  renderActionBtn,
}) => {
  const headerRow = headers.map((header) => {
    return (
      <div key={header.id} className={`cell ${header.width}`}>
        {header.title}
      </div>
    );
  });

  const dataRows = documentData.map((document: Document) => {
    const row = headers.map((header: Column, idx) => {
      if (document.hasOwnProperty(header.id)) {
        if (header.id === "action") {
          // return renderActionBtn(document.id);

          return (
            <div
              className={`cell ${header.width}`}
              key={document.id + header.id}
            >
              {renderActionBtn(document.id)}
            </div>
          );
        }

        if (header.id === "updatedOn") {
          const date = getKeyValue(document)(header.id as keyof Document);

          if (typeof date === "number") {
            return (
              <div
                className={`cell ${header.width}`}
                key={document.id + header.id}
              >
                {getTimeLabel(date)}
              </div>
            );
          }
        }

        return (
          <div className={`cell ${header.width}`} key={document.id + header.id}>
            {getKeyValue(document)(header.id as keyof Document)}
          </div>
        );
      }
    });

    return (
      <div className="row" key={document.id}>
        {row}
      </div>
    );
  });

  return (
    <div className="DocumentDataTable">
      <div className="header">{headerRow}</div>
      <div className="data">{dataRows}</div>
    </div>
  );
};
