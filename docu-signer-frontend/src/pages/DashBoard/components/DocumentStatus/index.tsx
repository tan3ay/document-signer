import React, { FC, useContext } from "react";
import { useHistory } from "react-router";

import "./index.scss";

interface IDocumentStatusItem {
  icon: string;
  header: string;
  detail: string;
  color: string;
}

export const DocumentStatusItem: FC<IDocumentStatusItem> = ({
  icon,
  header,
  detail,
  color,
}) => {
  return (
    <div className="DocumentStatusItem">
      <div className={`container ${color}`}>
        <div className="icon">{icon}</div>
        <div className="data">
          <div className="header">{header}</div>
          <div className="detail">{detail}</div>
        </div>
      </div>
    </div>
  );
};
