import { FC, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { resetProject } from "../../../../state/projectReducer";
import { RootState } from "../../../../state/store";

export const CreateDocument: FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const name = useSelector((state: RootState) => state.user.name);
  const handleCreateNew = useCallback(() => {
    dispatch(resetProject());
    history.push("/createProject");
  }, []);

  return (
    <div className="container v s-6">
      <div className="create-document">
        <div className="header">Hi, {name}</div>
        <div className="details">Let's Begin new document, shall we? </div>
        <div className="button-container">
          <button className="btn primary" onClick={handleCreateNew}>
            Create A Document
          </button>
          <button className="btn secondary">Use A Template</button>
        </div>
      </div>
    </div>
  );
};
