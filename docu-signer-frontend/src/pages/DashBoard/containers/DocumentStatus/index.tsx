import { FC } from "react";
import { useSelector } from "react-redux";
import { Document } from "../../../../model/document";
import { RootState } from "../../../../state/store";
import { DocumentStatusItem } from "../../components/DocumentStatus";
import helper from "../../../../util/helper";

export const DocumentStatus: FC = () => {
  const userDocuments: Document[] = useSelector(
    (state: RootState) => state.userDocuments.documents
  );
  const categories = ["all", "pending", "expired", "signed"];
  const documentCategories = helper.getDocumentsByStatus(userDocuments);
  const categoriesVo = helper.getDocumentCategoriesVo();
  const statusListItems = categories.map((category) => {
    const title = categoriesVo.get(category);
    if (title)
      return (
        <DocumentStatusItem
          key={"status-" + category}
          icon=""
          color={helper.getColorForCategory(category)}
          header={title}
          detail={"" + documentCategories.get(title)?.length + " documents"}
        />
      );
  });
  return <div className="container v s-6">{statusListItems}</div>;
};
