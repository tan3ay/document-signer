import { FC } from "react";
import { useSelector } from "react-redux";
import { Document } from "../../../../model/document";
import { RootState } from "../../../../state/store";
import { DocumentListPanel } from "../../components/documentListPanel";

export const DocumentLists: FC = () => {
  const userDocuments: Document[] = useSelector(
    (state: RootState) => state.userDocuments.documents
  );
  const assignedDocuments: Document[] = useSelector(
    (state: RootState) => state.assignedDocuments.documents
  );
  return (
    <div className="container h">
      <div className="container s-2">
        <DocumentListPanel
          header="Assign to Me"
          details="Document that reuire your attenssion"
          columnHeaders={[
            { id: "name", title: "Name", width: "w-6" },
            { id: "action", title: "Action", width: "w-4" },
          ]}
          documentData={assignedDocuments}
          isAssignedList
        />
      </div>

      <div className="container s-4 assigned-documents">
        <DocumentListPanel
          header="Recent"
          details="Review your recent documents"
          columnHeaders={[
            { id: "name", title: "Name", width: "w-4" },
            { id: "status", title: "Status", width: "w-2" },
            { id: "updatedOn", title: "Last Modified", width: "w-2" },
            { id: "action", title: "Action", width: "w-2" },
          ]}
          documentData={userDocuments}
        />
      </div>
    </div>
  );
};
