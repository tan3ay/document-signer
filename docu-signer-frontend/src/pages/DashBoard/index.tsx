import { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { NavBar } from "../../components/NavBar";

import { fetchUserDocuments } from "../../state/userDocumentReducer";
import { fetchAssignedDocuments } from "../../state/assignedDocumentReducer";
import { RootState } from "../../state/store";
import { CreateDocument } from "./containers/CreateDocument";
import { DocumentLists } from "./containers/DocumentLists";
import { DocumentStatus } from "./containers/DocumentStatus";

import "./index.scss";

export const DashBoard: FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const email: string = useSelector((state: RootState) => state.user.email);
  useEffect(() => {
    dispatch(fetchUserDocuments(email));
    dispatch(fetchAssignedDocuments(email));
  }, []);

  return (
    <main className="DashBoard">
      <div className="page-header">
        <NavBar />
      </div>
      <div className="content">
        <CreateDocument />
        <DocumentStatus />
        <DocumentLists />
      </div>
    </main>
  );
};
