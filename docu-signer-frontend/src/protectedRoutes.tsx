import { useDispatch } from "react-redux";
import { Redirect, Route, RouteProps } from "react-router";
import { onUserLoggedIn } from "./state/userReducer";

export type ProtectedRouteProps = {
  authenticationPath: string;
} & RouteProps;

export default function ProtectedRoute({
  authenticationPath,
  ...routeProps
}: ProtectedRouteProps) {
  const dispatch = useDispatch();
  const login = localStorage.getItem("login");
  let isAuthenticated = false;
  if (login) {
    const loginData = JSON.parse(login);
    if (loginData) {
      isAuthenticated = true;
      const userData = JSON.parse(atob(loginData.token.split(".")[1]));
      dispatch(
        onUserLoggedIn({
          email: userData.email,
          name: userData.name,
          loginSuccess: true,
        })
      );
    }
  }
  if (isAuthenticated) {
    return <Route {...routeProps} />;
  } else {
    return <Redirect to={{ pathname: authenticationPath }} />;
  }
}
