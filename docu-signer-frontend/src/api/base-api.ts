import axios from "axios";

// axios.interceptors.request.use(
//   async (config) => {
//     const login = localStorage.getItem("login");
//     const loginData = JSON.parse(login!);

//     config.headers = {
//       Authorization: `Bearer ${loginData.token}`,
//       Accept: "application/json",
//       "Content-Type": "application/x-www-form-urlencoded",
//     };
//     return config;
//   },
//   (error) => {
//     Promise.reject(error);
//   }
// );

export const API = {
  baseURL: "",
  //baseURL:'https://8z6p9dsdasdayyd55.execute-api.us-east-2.amazonaws.com/dev',

  getURL(URL: string): string {
    return this.baseURL + URL;
  },

  async makeGetRequest(URL: string, param: any) {
    return await axios.get(this.getURL(URL), {
      params: param,
      withCredentials: true,
    });
  },

  async makePostRequest(URL: string, requestBody: any, headers?: any) {
    return await axios.post(this.getURL(URL), requestBody);
  },

  async makePutRequest(URL: string, requestBody: any, headers?: any) {
    return await axios.put(this.getURL(URL), requestBody);
  },

  async makePatchRequest(URL: string, requestBody: any, headers?: any) {
    return await axios.patch(this.getURL(URL), requestBody);
  },
};
