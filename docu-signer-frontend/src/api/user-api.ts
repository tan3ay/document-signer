import { Document } from "../model/document";
import { API } from "./base-api";

const USER_ENDPOINT = "/users";

export const UserApi = {
  async login(email: string, password: string) {
    const data = {
      email,
      password,
    };
    const headers = {
      credentials: "include",
    };

    return await API.makePostRequest(USER_ENDPOINT + "/login", data, headers);
  },
};
