import { Document } from "../model/document";
import { API } from "./base-api";

const DOCUMENT_ENDPOINT = "/documents";

export const DocumentAPI = {
  async getUserDocuments(email: string) {
    const param = {
      ownerEmail: email,
    };
    return await API.makeGetRequest(DOCUMENT_ENDPOINT, param);
  },

  async getDocuments() {
    const param = {};
    return await API.makeGetRequest(DOCUMENT_ENDPOINT, param);
  },

  async getDocumentById(id: string) {
    const param = {};
    return await API.makeGetRequest(DOCUMENT_ENDPOINT + "/" + id, param);
  },

  async getSignerDocuments(email: string) {
    const param = {
      signerEmail: email,
      status: "pending",
    };
    return await API.makeGetRequest(DOCUMENT_ENDPOINT, param);
  },

  async postDocuments(document: Document[]) {
    return await API.makePostRequest(DOCUMENT_ENDPOINT, document);
  },

  async patchDocument(documentId: string, object: any) {
    return await API.makePatchRequest(
      DOCUMENT_ENDPOINT + "/" + documentId,
      object
    );
    //return await this.getDocuments();
  },
};
