import { Document } from "./document";

export interface Project {
  documents: Document[];
  name: string;
  createdOn: Date;
  updatedOn: Date;
}
