import { UserDto } from "./user";

export interface Document {
  id: string;
  name: string;
  // 'desc':string,
  type: string;
  ownerEmail: string;
  status: string;
  isSignatureRequired: boolean;
  signerName: string;
  signerEmail: string;
  projectName: string;
  action: string;
  updatedOn: Number;
}

export interface DocumentStatusVo {
  key: string;
  value: string;
}

export enum DOCUMENT_STATUS {
  PENDING = "pending",
  SIGNED = "signed",
  EXPIRED = "expired",
}

export interface DocumentApiResponse {
  _id: string;
  __v: number;
  id: string;
  name: string;
  // 'desc':string,
  type: string;
  ownerEmail: string;
  status: string;
  isSignatureRequired: boolean;
  signerName: string;
  signerEmail: string;
  projectName: string;
  updatedOn: Number;
}

export const mapResponseToEntity = (
  response: DocumentApiResponse
): Document => {
  return {
    id: response._id,
    name: response.name,
    // desc:response.desc,
    type: response.type,
    ownerEmail: response.ownerEmail,
    status: response.status,
    isSignatureRequired: response.isSignatureRequired,
    signerName: response.signerName,
    signerEmail: response.signerEmail,
    projectName: response.projectName,
    action: "",
    updatedOn: response.updatedOn,
  };
};
export const getKeyValue =
  <T extends object, U extends keyof T>(obj: T) =>
  (key: U) =>
    obj[key];
