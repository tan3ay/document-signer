import { FC } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import logo from "../../public/assets/logo.png";

import "./index.scss";
export const NavBar: FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const src = "../assets/logo.png";
  return (
    <div className="NavBar">
      <div className="logo-header">
        <img className="image" alt="" src={logo} width="80" height="40" />
      </div>
      <div className="menu-container left">
        <div className="menu-item">Dashboard</div>
        <div className="menu-item">Project</div>
        <div className="menu-item">Verify</div>
        <div className="menu-item">Contact</div>
      </div>
      <div
        className="menu-container right"
        onClick={(e) => {
          localStorage.clear();
          // TODO Resent Lists
          history.push("/login");
        }}
      >
        Logout
      </div>
    </div>
  );
};
