import React, { useContext, useEffect, useState } from "react";
import "./App.css";
import { LoginPage } from "./pages/Login";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { DashBoard } from "./pages/DashBoard";
import { DocumentApplication } from "./pages/DocumentApplication";
import ProtectedRoute from "./protectedRoutes";
import DocView from "./pages/DocView";
import EmailView from "./pages/EmailView";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <ProtectedRoute
            path="/dashboard"
            authenticationPath="/login"
            component={DashBoard}
          ></ProtectedRoute>
          <ProtectedRoute
            path="/createProject"
            authenticationPath="/login"
            component={DocumentApplication}
          ></ProtectedRoute>
          <Route path="/login" component={LoginPage} />
          <Route path="/emailView" component={EmailView}></Route>
          <Route
            path="/docView/:documentId/:isSinger"
            component={DocView}
          ></Route>
          <Route path="/" component={LoginPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
