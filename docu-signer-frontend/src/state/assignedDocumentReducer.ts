import {
  Document,
  DocumentApiResponse,
  mapResponseToEntity,
} from "../model/document";
import { DocumentAPI } from "../api/document-api";
import { Dispatch } from "react";
import DocumentService from "../util/helper";

export enum ASSIGNED_DOCUMENT_LIST_ACTION {
  FETCH_ASSIGNED_DOCUMENT_SUCCESS = "FETCH_ASSIGNED_DOCUMENT_SUCCESS",
  FETCH_ASSIGNED_DOCUMENT_FAILURE = "FETCH_ASSIGNED_DOCUMENT_FAILURE",
  FETCH_ASSIGNED_DOCUMENT = "FETCH_ASSIGNED_DOCUMENT",
}

interface assignedDocumentLoaded {
  type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_SUCCESS;
  payload: Document[];
}

interface assignedDocumentLoadFailed {
  type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_FAILURE;
}

interface assignedFetchDocument {
  type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT;
}

export type AssignedDocumentDispatchTypes =
  | assignedDocumentLoaded
  | assignedDocumentLoadFailed
  | assignedFetchDocument;

export const fetchAssignedDocuments = (email: string) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch(fetchAssignedDocumentsRequest());
    try {
      const documents: DocumentApiResponse[] = (
        await DocumentAPI.getSignerDocuments(email)
      ).data;
      const documentsVos: Document[] = documents.map((document) => {
        return mapResponseToEntity(document);
      });
      // let documentsVosTest: Document[] =
      //   DocumentService.getUserAssignedDocument("1");

      dispatch(fetchAssignedDocumentsSuccess(documentsVos));
    } catch {
      dispatch(fetchAssignedDocumentsFailure());
    }
  };
};

export const fetchAssignedDocumentsRequest = () => {
  return (dispatch: Dispatch<AssignedDocumentDispatchTypes>) => {
    dispatch({
      type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT,
    });
  };
};

export const fetchAssignedDocumentsSuccess = (documents: Document[]) => {
  return (dispatch: Dispatch<AssignedDocumentDispatchTypes>) => {
    dispatch({
      type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_SUCCESS,
      payload: documents,
    });
  };
};

export const fetchAssignedDocumentsFailure = () => {
  return (dispatch: Dispatch<AssignedDocumentDispatchTypes>) => {
    dispatch({
      type: ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_FAILURE,
    });
  };
};

export interface DocumentState {
  loading: boolean;
  documents: Document[];
  error: string;
}

const initialState: DocumentState = {
  loading: false,
  documents: [] as Document[],
  error: "",
};

export const assignedDocumentReducer = (
  state: DocumentState = initialState,
  action: AssignedDocumentDispatchTypes
): DocumentState => {
  switch (action.type) {
    case ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT:
      return {
        ...state,
        loading: true,
      };
    case ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case ASSIGNED_DOCUMENT_LIST_ACTION.FETCH_ASSIGNED_DOCUMENT_SUCCESS:
      return {
        loading: false,
        documents: action.payload,
        error: "",
      };
    default:
      return state;
  }
};
