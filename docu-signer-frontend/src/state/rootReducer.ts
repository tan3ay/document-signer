import { combineReducers } from "redux";
import { assignedDocumentReducer } from "./assignedDocumentReducer";
import { emailReducer } from "./emailReducer";
import { userReducer } from "./userReducer";
import { projectReducer } from "./projectReducer";
import { userDocumentReducer } from "./userDocumentReducer";

export const reducers = combineReducers({
  userDocuments: userDocumentReducer,
  assignedDocuments: assignedDocumentReducer,
  newPrpject: projectReducer,
  email: emailReducer,
  user: userReducer,
});
