import { Dispatch } from "react";
export type User = {
  email: string;
  name: string;
  loginSuccess: boolean;
};

enum USER_ACTION {
  USER_LOGGEDIN = "USER_LOGGEDIN",
}
type UserLoggedIn = {
  type: USER_ACTION.USER_LOGGEDIN;
  payload: User;
};

export const onUserLoggedIn = (user: User) => {
  return (dispatch: Dispatch<UserLoggedIn>) => {
    dispatch({
      type: USER_ACTION.USER_LOGGEDIN,
      payload: user,
    });
  };
};

const initialState: User = {
  email: "",
  name: "juno",
  loginSuccess: false,
};
export const userReducer = (
  state: User = initialState,
  action: UserLoggedIn
) => {
  switch (action.type) {
    case USER_ACTION.USER_LOGGEDIN:
      return action.payload;
    default:
      return state;
  }
};
