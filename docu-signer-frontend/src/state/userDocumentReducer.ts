import {
  Document,
  DocumentApiResponse,
  mapResponseToEntity,
} from "../model/document";
import { Dispatch } from "react";
import { DocumentAPI } from "../api/document-api";
import helper from "../util/helper";

export const fetchUserDocuments = (email: string) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch(fetchDocumentsRequest());
    try {
      const documents: DocumentApiResponse[] = (
        await DocumentAPI.getUserDocuments(email)
      ).data;

      const documentsVos: Document[] = documents.map((document) => {
        return mapResponseToEntity(document);
      });

      // let documentsVosTest: Document[] = helper.getUserAssignedDocument("1");
      dispatch(fetchDocumentsSuccess(documentsVos));
    } catch {
      dispatch(fetchDocumentsFailure());
    }
  };
};
export const fetchDocumentsRequest = () => {
  return (dispatch: Dispatch<DocumentDispatchTypes>) => {
    dispatch({
      type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT,
    });
  };
};

export const fetchDocumentsSuccess = (documents: Document[]) => {
  return (dispatch: Dispatch<DocumentDispatchTypes>) => {
    dispatch({
      type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_SUCCESS,
      payload: documents,
    });
  };
};

export const fetchDocumentsFailure = () => {
  return (dispatch: Dispatch<DocumentDispatchTypes>) => {
    dispatch({
      type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_FAILURE,
    });
  };
};

export type DocumentDispatchTypes =
  | fetchDocument
  | documentLoaded
  | documentLoadFailed;

interface documentLoaded {
  type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_SUCCESS;
  payload: Document[];
}

interface documentLoadFailed {
  type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_FAILURE;
}

interface fetchDocument {
  type: USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT;
}

export enum USER_DOCUMENT_LIST_ACTION {
  FETCH_USER_DOCUMENT_SUCCESS = "FETCH_USER_DOCUMENT_SUCCESS",
  FETCH_USER_DOCUMENT_FAILURE = "FETCH_USER_DOCUMENT_FAILURE",
  FETCH_USER_DOCUMENT = "FETCH_USER_DOCUMENT",
}

export interface DocumentState {
  loading: boolean;
  documents: Document[];
  error: string;
}
const initialState: DocumentState = {
  loading: false,
  documents: [] as Document[],
  error: "",
};

export const userDocumentReducer = (
  state: DocumentState = initialState,
  action: DocumentDispatchTypes
): DocumentState => {
  switch (action.type) {
    case USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT:
      return {
        ...state,
        loading: true,
      };
    case USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case USER_DOCUMENT_LIST_ACTION.FETCH_USER_DOCUMENT_SUCCESS:
      return {
        loading: false,
        documents: action.payload,
        error: "",
      };
    default:
      return state;
  }
};
