import { Dispatch } from "react";

export type Email = {
  subject: string;
  message: string;
};

enum EMAIL_ACTION {
  UPDATE_EMAIL = "UPDATE EMAIL",
}
type UpdateEmail = {
  type: EMAIL_ACTION;
  payload: Email;
};

export const updateEmail = (email: Email) => {
  return (dispatch: Dispatch<UpdateEmail>) => {
    dispatch({
      type: EMAIL_ACTION.UPDATE_EMAIL,
      payload: email,
    });
  };
};

const initialState: Email = {
  subject: "",
  message: "",
};
export const emailReducer = (
  state: Email = initialState,
  action: UpdateEmail
) => {
  switch (action.type) {
    case EMAIL_ACTION.UPDATE_EMAIL:
      return action.payload;
    default:
      return state;
  }
};
