import { Project } from "../model/project";
import {
  DocumentApiResponse,
  Document,
  mapResponseToEntity,
} from "../model/document";
import { Dispatch } from "react";
import helper from "../util/helper";
import { DocumentAPI } from "../api/document-api";

export enum PROJECT_ACTION {
  DOCUMENT_ADDED = "DOCUMENT_ADDED",
  DOCUMENT_REMOVED = "DOCUMENT_REMOVED",
  DOCUMENT_UPDATED = "DOCUMENT_UPDATED",
  DOCUMENTS_UPDATED = "DOCUMENTS_UPDATED",
  PROCESS_ADDED = "PROCESS_ADDED",
  SUBMIT_PROJECT = "SUBMIT_PROJECT",
  PROJECT_UPDATED = "PROJECT_UPDATED",
  PROJECT_RESET = "PROJECT_RESET",
  PROJECT_SUBMIT_SUCCESS = "PROJECT_SUBMIT_SUCCESS",
  PROJECT_SUBMIT_FAILURE = "PROJECT_SUBMIT_FAILURE",
}

interface NewDocumentAdded {
  type: PROJECT_ACTION.DOCUMENT_ADDED;
  payload: Document;
}

interface DocumentUpdated {
  type: PROJECT_ACTION.DOCUMENT_UPDATED;
  payload: Document;
}
interface DocumentsUpdated {
  type: PROJECT_ACTION.DOCUMENTS_UPDATED;
  payload: Document;
}

interface NewDocumentRemoved {
  type: PROJECT_ACTION.DOCUMENT_REMOVED;
  payload: Document;
}

interface NewProcessAdded {
  type: PROJECT_ACTION.PROCESS_ADDED;
  payload: Document;
}

interface SumbitProject {
  type: PROJECT_ACTION.SUBMIT_PROJECT;
  payload: Project;
}

interface SumbitProjectSuccess {
  type: PROJECT_ACTION.PROJECT_SUBMIT_SUCCESS;
  payload: Document[];
}

interface SumbitProjectFailure {
  type: PROJECT_ACTION.PROJECT_SUBMIT_FAILURE;
  payload: Project;
}

interface ProjectUpdated {
  type: PROJECT_ACTION.PROJECT_UPDATED;
  payload: Project;
}

interface ProjectReset {
  type: PROJECT_ACTION.PROJECT_RESET;
}
export type ProjectDispatchTypes =
  | ProjectUpdated
  | NewDocumentAdded
  | DocumentUpdated
  | DocumentsUpdated //TODO check here
  | NewProcessAdded
  | SumbitProject //TODO checek here
  | SumbitProjectSuccess
  | SumbitProjectFailure
  | NewDocumentRemoved
  | ProjectReset;

export const documentAdded = (ownerEmail: string, fileName: string) => {
  const document: Document = helper.createDocument(ownerEmail, fileName);
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.DOCUMENT_ADDED,
      payload: document,
    });
  };
};

export const submitProject = (project: Project) => {
  return async (dispatch: Dispatch<any>) => {
    try {
      const documents: DocumentApiResponse[] = (
        await DocumentAPI.postDocuments(project.documents)
      ).data;
      const documentsVos: Document[] = documents.map((document) => {
        return mapResponseToEntity(document);
      });
      dispatch(onProjectSubmitted(documentsVos));
    } catch {
      // dispatch(fetchAssignedDocumentsFailure());
    }
  };
};

export const projectUpdated = (project: Project) => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.PROJECT_UPDATED,
      payload: project,
    });
  };
};

export const documentRemoved = (document: Document) => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.DOCUMENT_REMOVED,
      payload: document,
    });
  };
};

export const documentUpdated = (document: Document) => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.DOCUMENT_UPDATED,
      payload: document,
    });
  };
};

export const onProcessAdded = (document: Document) => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.PROCESS_ADDED,
      payload: document,
    });
  };
};

export const onProjectSubmitted = (documents: Document[]) => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.PROJECT_SUBMIT_SUCCESS,
      payload: documents,
    });
  };
};

export const resetProject = () => {
  return (dispatch: Dispatch<ProjectDispatchTypes>) => {
    dispatch({
      type: PROJECT_ACTION.PROJECT_RESET,
    });
  };
};

const initialStateProject: ProjectState = {
  loading: false,
  project: {
    documents: [] as Document[],
    name: "",
    createdOn: new Date(),
    updatedOn: new Date(),
  },
};

export interface ProjectState {
  loading: boolean;
  project: Project;
}

export const projectReducer = (
  state: ProjectState = initialStateProject,
  action: ProjectDispatchTypes
): ProjectState => {
  switch (action.type) {
    case PROJECT_ACTION.PROJECT_UPDATED:
      return {
        ...state,
        project: action.payload,
      };
    case PROJECT_ACTION.DOCUMENT_ADDED:
      const newProject = { ...state.project };
      const newDocuments = [...newProject.documents];
      newDocuments.push(action.payload);
      newProject.documents = newDocuments;
      return {
        ...state,
        project: newProject,
      };
    case PROJECT_ACTION.DOCUMENT_UPDATED: {
      const newProject = { ...state.project };
      const index = newProject.documents.findIndex(
        (document) => document.id === action.payload.id
      );
      newProject.documents[index] = { ...action.payload };

      return {
        ...state,
        project: newProject,
      };
    }
    // case PROJECT_ACTION.DOCUMENTS_UPDATED: {
    //   const newProject = { ...state.project };
    //   const newDocuments: Document[] = [];
    //   newProject.documents.forEach((document) =>
    //     newDocuments.push({ ...document })
    //   );
    //   newProject.documents.splice(0, newDocuments.length, ...newDocuments);
    //   return {
    //     ...state,
    //     project: newProject,
    //   };
    // }

    case PROJECT_ACTION.DOCUMENT_REMOVED: {
      const newProject = { ...state.project };
      newProject.documents = state.project.documents
        .filter((document) => document.id !== action.payload.id)
        .map((document) => {
          return { ...document };
        });

      return {
        ...state,
        project: newProject,
      };
    }
    case PROJECT_ACTION.PROCESS_ADDED: {
      const newProject = { ...state.project };
      const index = newProject.documents.findIndex(
        (document) => document.id === action.payload.id
      );
      newProject.documents[index] = { ...action.payload };
      return {
        ...state,
        project: newProject,
      };
    }
    case PROJECT_ACTION.PROJECT_RESET:
    case PROJECT_ACTION.SUBMIT_PROJECT:
      return initialStateProject;
    case PROJECT_ACTION.PROJECT_SUBMIT_SUCCESS: {
      const newProject = { ...state.project };
      const newDocuments: Document[] = [];
      action.payload.forEach((document) => newDocuments.push({ ...document }));
      newProject.documents.splice(0, newDocuments.length, ...newDocuments);
      return {
        ...state,
        project: newProject,
      };
    }

    case PROJECT_ACTION.PROJECT_SUBMIT_FAILURE:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
};
