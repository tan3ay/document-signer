import { map } from "lodash";
import { Document, DOCUMENT_STATUS } from "../model/document";

export default {
  getUserAssignedDocument: function (userId: string) {
    const documents: Document[] = [];
    documents.push({
      id: "assigned1123",
      name: "Document 1",
      // desc:'string',
      type: "string",
      ownerEmail: "test@gmail.com",
      status: "pending",
      isSignatureRequired: true,
      signerEmail: "",
      signerName: "",
      projectName: "",
      action: "",
      updatedOn: new Date(2021, 11, 28, 13, 24, 0).valueOf(),
    });

    return documents;
  },

  createDocument(ownerEmail: string, fileName: string) {
    const document: Document = {
      id: new Date().valueOf() + "",
      name: fileName,
      type: "General",
      ownerEmail: ownerEmail,
      status: DOCUMENT_STATUS.PENDING.valueOf(),
      isSignatureRequired: true,
      signerEmail: "",
      signerName: "",
      projectName: "",
      action: "",
      updatedOn: new Date(2021, 11, 28, 13, 24, 0).valueOf(),
    };
    return document;
  },

  getDocumentsByStatus: function (documents: Document[]) {
    let map = new Map<string, Document[]>();
    let documentCategoriesVo = this.getDocumentCategoriesVo();
    documentCategoriesVo.forEach((value, key) => map.set(value, []));
    map.set(documentCategoriesVo.get("all")!, documents);
    let titleKey: string = "";
    documents.forEach((item) => {
      titleKey = documentCategoriesVo.get(item.status)!;
      if (map.get(titleKey)) {
        map.get(titleKey)?.push(item);
      } else {
        let currStatusDocs: Document[] = [];
        map.set(titleKey!, currStatusDocs);
        map.get(titleKey)?.push(item);
      }
    });
    return map;
  },
  getDocumentCategoriesVo: function () {
    let documentCategoriesVo = new Map<string, string>();
    documentCategoriesVo.set("pending", "PENDING >");
    documentCategoriesVo.set("expired", "EXPIRED >");
    documentCategoriesVo.set("signed", "COMPLETED >");
    documentCategoriesVo.set("all", "ALL DOCUMENTS >");
    return documentCategoriesVo;
  },
  getColorForCategory: function (category: string) {
    switch (category) {
      case "pending":
        return "warning";
      case "expired":
        return "error";
      case "signed":
        return "success";
      default:
        return "normal";
    }
  },
  dtoToEntity: function () {},
};
