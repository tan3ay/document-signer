const mongoose = require("mongoose");
const documentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  ownerEmail: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  signerName: {
    type: String,
    required: true,
  },
  signerEmail: {
    type: String,
    required: true,
  },
  isSignatureRequired: {
    type: Boolean,
    required: true,
    default: false,
  },
  projectId: {
    type: String,
    required: true,
  },
  projectName: {
    type: String,
    required: true,
  },
  updatedOn: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("Document", documentSchema);
