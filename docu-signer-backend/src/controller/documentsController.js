const { request } = require("express");
const Document = require("../models/document");
const mongoose = require("mongoose");

const searchDocument = async (req, res) => {
  try {
    let documents;
    const { signerEmail, ownerEmail, status } = req.query;
    const validParam = true;
    // Get By Fields

    if (signerEmail && status) {
      if (isValidEmail(signerEmail) && isValidStatus(status)) {
        documents = await Document.find({ signerEmail, status });
      } else {
        res.status(400).json({ message: "Invalid parameter values" });
      }
    } else if (ownerEmail) {
      if (isValidEmail(ownerEmail)) {
        documents = await Document.find({ ownerEmail });
      } else {
        res.status(400).json({ message: "Invalid parameter values" });
      }
    } else if (Object.keys(req.query).length === 0) {
      // Get all Documents
      documents = await Document.find({});
    } else {
      res.status(400).json({ message: "Invalid parameter values" });
    }
    res.status(200).json(documents);
  } catch (error) {
    res.status(500).json({ message: "Server Error" });
  }
};

const getDocumentById = async (req, res) => {
  try {
    if (mongoose.Types.ObjectId.isValid(req.params.id)) {
      const document = await Document.findById(req.params.id);
      res.status(200).json(document);
    } else {
      res.status(400).json({ message: "Invalid parameter values" });
    }
  } catch (error) {
    res.status(500).json({ message: "Server Error" });
  }
};

const saveDocuments = async (req, res) => {
  const document = req.body.map((element) => {
    return {
      ...element,
      projectId: new Date().valueOf(),
      updatedOn: new Date().valueOf(),
    };
  });
  try {
    const doc = await Document.insertMany(document);
    res.status(200).json(doc);
  } catch (error) {
    res.status(500).json({ message: "Fail to save the documents" });
  }
};

const updateDocumentById = async (req, res) => {
  var _id = req.params.id;

  try {
    if (mongoose.Types.ObjectId.isValid(_id)) {
      const doc = await Document.findByIdAndUpdate(_id, req.body, {
        new: true,
      });
      res.status(200).json(doc);
      // res.status(204).send();
    } else {
      res.status(400).json({ message: "Invalid parameter values" });
    }
  } catch (error) {
    res.status(404).json({ message: "Fail to update the documents" });
  }
};

const isValidEmail = (email) => {
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (email && email.match(mailformat)) {
    return true;
  }
  return false;
};

const isValidStatus = (status) => {
  if (
    status &&
    (status === "pending" || status === "signed" || status === "expired")
  ) {
    return true;
  }
  return false;
};

module.exports = {
  searchDocument,
  getDocumentById,
  saveDocuments,
  updateDocumentById,
};
