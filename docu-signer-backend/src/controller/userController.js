const User = require("../models/user");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const registerUser = async (req, res) => {
  try {
    const user = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    });
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    const userVo = await user.save();
    res.status(201).json(userVo);
  } catch (err) {
    res.status(500).send("Fail to register the user. Please try again later.");
  }
};

const loginUser = async (req, res) => {
  try {
    if (!isValidEmail(req.body.email)) {
      res.status(400).json({ message: "Invalid parameter values" });
    }

    const user = await User.findOne({
      email: req.body.email,
    });
    if (user) {
      const validPassword = await bcrypt.compare(
        req.body.password,
        user.password
      );

      if (validPassword) {
        const token = jwt.sign(
          {
            email: user.email,
            name: user.name,
          },
          "mysecret" // should come from environment properties
        );
        res.cookie("token", token, { httpOnly: true });
        res.status(200).json({ status: "ok", token: token });
      } else {
        res.status(401).json({ error: "Invalid Password" });
      }
    } else {
      res.status(401).json({ error: "User does not exist" });
    }
  } catch (err) {
    res.status(500).send("Internal server error.");
  }
};

const isValidEmail = (email) => {
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (email && email.match(mailformat)) {
    return true;
  }
  return false;
};

module.exports = {
  registerUser,
  loginUser,
};
