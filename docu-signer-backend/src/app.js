const express = require("express");
const mongoose = require("mongoose");
const url = "mongodb://localhost/docuSignDB";
const cors = require("cors");
const cookieParser = require("cookie-parser");
const jwt = require("jsonwebtoken");

const app = express();
app.use(express.json());

app.use(cors());

mongoose.connect(url, { useNewUrlParser: true });
const con = mongoose.connection;

con.on("open", () => {
  console.log("connected...");
});

// Authorization for all incoming request
app.use("*", function (req, res, next) {
  // Login
  if (req.originalUrl === "/users/login") return next();
  // User Registration API
  if (req.originalUrl === "/users/" || req.originalUrl === "/users")
    return next();

  if (
    req.originalUrl.startsWith("/documents") &&
    (req.method === "GET" || req.method === "PATCH")
  )
    return next();
  //   let token = req.headers["authorization"] || req.headers["x-access-token"];
  let token = req.headers.cookie;
  if (token && token.startsWith("token=")) {
    token = token.slice(6, token.length);
    jwt.verify(token, "mysecret", (err, decoded) => {
      if (err) {
        res.status(401).json({
          error: "Token invalid",
        });
      } else {
        next();
      }
    });
  } else {
    res.status(400).json({
      error: "Token missing",
    });
  }
});

const documentRoutes = require("./routes/documents");
app.use("/documents", documentRoutes);

const userRoutes = require("./routes/users");
app.use("/users", userRoutes);

app.use(cookieParser());

app.listen(9000, () => {
  console.log("Server started");
});

// app.use(function (req, res, next) {
//   res.header("Content-Type", "application/json;charset=UTF-8");
//   res.header("Access-Control-Allow-Credentials", true);
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept"
//   );
//   next();
// });
