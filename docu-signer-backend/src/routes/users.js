const express = require("express");
const router = express.Router();
const User = require("../models/user");

const { registerUser, loginUser } = require("../controller/userController");

router.post("/", registerUser);
router.post("/login", loginUser);

module.exports = router;
