const express = require("express");
const router = express.Router();
const Documents = require("../models/document");
const authorization = require("../app");
const {
  searchDocument,
  getDocumentById,
  saveDocuments,
  updateDocumentById,
} = require("../controller/documentsController");

router.get("/", searchDocument);
router.post("/", saveDocuments);
router.get("/:id", getDocumentById);
router.patch("/:id", updateDocumentById);

module.exports = router;
